import React, {Component} from 'react';
import {
  Alert,
  ErrorMessage,
  StatusBar,
  Platform,
  SafeAreaView,
  KeyboardAvoidingView,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  View
} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Lottie from 'lottie-react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Header } from 'react-native-elements';

import logo from '../assets/logo.png';
import submit from '../assets/submit.json'

export class Main extends Component{
  static navigationOptions = {
    header: null,
  };
  state = {
    valor: '',
    description: '',
    date: '',
    error: '',
  };

  setValor = (valor) => {
    this.setState({ valor });
  };

  setDescription = (description) => {
    this.setState({ description });
  };

  setDate = (date) => {
    this.setState({ date });
  };

  handleSignInPress = async () => {
    if(this.state.valor.length === 0 || this.state.description.length === 0 || this.state.date.length === 0) {
      Alert.alert('All fields required')
    } else {
      <Lottie source={submit} autoPlay loop={false}/>
      Alert.alert('Successful registration');
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>

        <StatusBar
          barStyle="light-content"
          backgroundColor={styles.container.backgroundColor}
        />

        <Header
          containerStyle={{
            backgroundColor: '#3cb371'
          }}
          placement="center"
          leftComponent={{ icon: 'menu', color: '#fff', size: 40 }}
          centerComponent={{text: 'Cadastro', style: {color: '#fff', fontSize: 18, fontWeight: 'bold', justifyContent: 'center'}}}
          rightComponent={{ icon: 'home', color: '#fff', size: 40}}
        />
 
        <Image source={logo} style={styles.logo} />

        <KeyboardAvoidingView enabled={Platform.OS === 'android'} behavior="padding"  style={styles.form}>
          <Text style={styles.label}>  INFORME VALOR *</Text>
          <TextInput
            style={styles.input}
            placeholder="$"
            value={this.state.valor}
            onChangeText={this.setValor}
            placeholderTextColor="#90ee90"
            keyboardType="number-pad"
          />
  
          <Text style={styles.label}>   *</Text>
          <TextInput
            style={styles.input}
            placeholder="Cliente ou Serviço"
            value={this.state.description}
            onChangeText={this.setDescription}
            placeholderTextColor="#90ee90"
            autoCapitalize="words"
            autoCorrect={false}
          />
  
          <Text style={styles.label}>  DATA *</Text>
          <TextInput
            style={styles.input}
            placeholder="00/00/0000"
            value={this.state.date}
            onChangeText={this.setDate}
            placeholderTextColor="#90ee90"
            autoCapitalize="words"
            autoCorrect={false}
            keyboardType="phone-pad"
          />
          {this.state.error.length !== 0 && <ErrorMessage>{this.state.error}</ErrorMessage>}
          <TouchableOpacity
            onPress={this.handleSignInPress}
            style={styles.button}>
            <Text style={styles.buttonText}>Cadastrar</Text>
          </TouchableOpacity>
        </ KeyboardAvoidingView>
      </ SafeAreaView>
    );
  } 
}

export class Despesa extends Component{
  render () {
    return (
      <View style={styles.view}>
        <Text>Despesa</Text>
      </View>
    )
  }
}

export default createBottomTabNavigator({
  Receita: {
    screen: Main,
    navigationOptions: {
      tabBarLabel: 'Receita',
      tabBarIcon: ({tintColor}) => (
        <Icon name="credit-card" size={24} />
      )
    }
  },
  Despesa: {
    screen:Despesa,
    navigationOptions: {
      tabBarLabel: 'Despesa',
      tabBarIcon:({tintColor}) => (
        <Icon name="warning" size={24} />
      )
    }
  }
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3cb371'
  },

  form: {
    flex: 1,
    alignSelf: 'stretch',
    paddingHorizontal: 30,
    marginTop: 100,
  },

  label: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 5,
  },

  input: {
    borderWidth: 1,
    borderColor: '#ddd',
    paddingHorizontal: 20,
    fontSize: 16,
    color: '#fff',
    height: 44,
    marginBottom: 20,
    borderRadius: 2
  },

  button: {
    height: 42,
    backgroundColor: '#90ee90',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },

  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },

  logo: {
    marginTop: 70,
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 4
  },

});